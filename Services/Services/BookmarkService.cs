﻿using Data;
using Entity;
using Microsoft.EntityFrameworkCore;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class BookmarkService : IBookmarkService
    {
        private ReadLaterDataContext _ReadLaterDataContext;
        public BookmarkService(ReadLaterDataContext readLaterDataContext) 
        {
            _ReadLaterDataContext = readLaterDataContext;            
        }

        public Bookmark CreateBookmark(BookmarkInputModel bookmark)
        {
            var category = _ReadLaterDataContext.Categories.FirstOrDefault(x => x.Name == bookmark.CategoryName);
            var newCategory = new Category();

            if (category == null)
            {
                newCategory.Name = bookmark.CategoryName;

                _ReadLaterDataContext.Categories.Add(newCategory);
            }
            else
            {
                newCategory = category;
            }

            var newBookmark = new Bookmark
            {
                URL = bookmark.URL,
                ShortDescription = bookmark.ShortDescription,
                Category = newCategory,
                CreateDate = DateTime.Now
            };

            _ReadLaterDataContext.Add(newBookmark);
            _ReadLaterDataContext.SaveChanges();
            return newBookmark;
        }

        public void UpdateBookmark(Bookmark bookmark)
        {
            _ReadLaterDataContext.Update(bookmark);
            _ReadLaterDataContext.SaveChanges();
        }

        public List<Bookmark> GetBookmarks()
        {
            return _ReadLaterDataContext.Bookmark.Include(x => x.Category).ToList();
        }

        public Bookmark GetBookmark(int Id)
        {
            return _ReadLaterDataContext.Bookmark.Where(c => c.ID == Id).Include(x => x.Category).FirstOrDefault();
        }

        public Bookmark GetBookmark(string URL)
        {
            return null;
        }

        public void DeleteBookmark(Bookmark bookmark)
        {
            _ReadLaterDataContext.Bookmark.Remove(bookmark);
            _ReadLaterDataContext.SaveChanges();
        }
    }
}
