﻿using System;
namespace Services.Models
{
    public class BookmarkInputModel
    {
        public string URL { get; set; }

        public string ShortDescription { get; set; }

        public string CategoryName { get; set; }
    }
}
