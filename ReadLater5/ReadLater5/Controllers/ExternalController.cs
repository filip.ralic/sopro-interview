﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity;
using Microsoft.AspNetCore.Mvc;
using Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReadLater5.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ExternalController : Controller
    {
        private IBookmarkService _bookmarkService;

        public ExternalController(IBookmarkService bookmarkService)
        {
            _bookmarkService = bookmarkService;
        }

        // GET: /<controller>/
        [HttpGet]
        public IEnumerable<List<Bookmark>> Get()
        {
            List<Bookmark> model = _bookmarkService.GetBookmarks();

            return Enumerable.Range(1,5).Select(index => model).ToArray();
        }
    }
}
