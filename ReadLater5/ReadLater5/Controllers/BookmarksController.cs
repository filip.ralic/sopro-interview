﻿using Entity;
using Microsoft.AspNetCore.Mvc;
using Services.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReadLater5.Controllers
{
    //[ApiController]
    //[Route("[controller]")]
    public class BookmarksController : Controller
    {
        private IBookmarkService _bookmarkService;

        public BookmarksController(IBookmarkService bookmarkService)
        {
            _bookmarkService = bookmarkService;
        }

        [HttpGet]
        public IEnumerable<Bookmark> Get()
        {
            List<Bookmark> model = _bookmarkService.GetBookmarks();
            return model.ToArray();
        }

    // GET: Bookmarks
    public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Index", "Home");

            List<Bookmark> model = _bookmarkService.GetBookmarks();
            return View(model);
        }

        // GET: Bookmarks/Create
        public IActionResult Create()
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Index", "Home");

            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BookmarkInputModel bookmark)
        {
            if (ModelState.IsValid)
            {
                _bookmarkService.CreateBookmark(bookmark);
                return RedirectToAction("Index");
            }

            return View(bookmark);
        }

        // GET: Bookmark/Edit/5
        public IActionResult Edit(int? id)
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);
            }
            Bookmark bookmark = _bookmarkService.GetBookmark((int)id);
            if (bookmark == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);
            }
            return View(bookmark);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Bookmark bookmark)
        {
            if (ModelState.IsValid)
            {
                _bookmarkService.UpdateBookmark(bookmark);
                return RedirectToAction("Index");
            }
            return View(bookmark);
        }

        // GET: Bookmark/Details/5
        public IActionResult Details(int? id)
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);
            }
            Bookmark bookmark = _bookmarkService.GetBookmark((int)id);
            if (bookmark == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);
            }
            return View(bookmark);

        }

        // GET: Bookmark/Delete/5
        public IActionResult Delete(int? id)
        {
            if (User.Identity.IsAuthenticated == false)
                return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);
            }
            Bookmark bookmark = _bookmarkService.GetBookmark((int)id);
            if (bookmark == null)
            {
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);
            }
            return View(bookmark);
        }

        // POST: Bookmark/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Bookmark bookmark = _bookmarkService.GetBookmark(id);
            _bookmarkService.DeleteBookmark(bookmark);
            return RedirectToAction("Index");
        }

        // POST: Bookmark/BookmarkPage
        [HttpPost, ActionName("BookmarkPage")]
        [ValidateAntiForgeryToken]
        public IActionResult BookmarkPage()
        {
            return RedirectToAction("Index");
        }

    }
}
